/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Paint;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 *
 * @author RafaelZamora
 */
public class Linea extends Figura{
    Point p1, p2;

    public Linea(Point p1, Point p2, Color color) {
        this.p1 = new Point(p1.x, p1.y);
        this.p2 = new Point(p2.x, p2.y);
        this.color = color;
    }
    
    @Override
    void Dibujar(Graphics2D g2d){
        if(color != null){
            g2d.setColor(color);
        }
        g2d.drawLine(p1.x, p1.y, p2.x, p2.y);
    }
    
    @Override
    void Rotar(Graphics2D g2d){
        tetha += 15;
        g2d.rotate(Math.toRadians(tetha));
        Dibujar(g2d);
    }
    
    @Override
    void EscalarMas(Graphics2D g2d){
        zoom += 0.2d;
        g2d.scale(zoom, zoom);
        Dibujar(g2d);
    }
    
    @Override
    void EscalarMenos(Graphics2D g2d){
        zoom -= 0.2d;
        g2d.scale(zoom, zoom);
        Dibujar(g2d);
    }
    
}
