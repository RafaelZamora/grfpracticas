/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Paint;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author reymi
 */
public class Poligono extends Figura {
    
    Color color;
    ArrayList<Point> Puntos;
    
    public Poligono(ArrayList<Point> Puntos, Color color){
        this.Puntos = Puntos;
        this.color = color;
    }
    
    @Override
    public void Dibujar(Graphics2D g2d){
        for(int i = 0; i < Puntos.size(); i++){
            try {
                g2d.drawLine(Puntos.get(i).x, Puntos.get(i).y, Puntos.get(i+1).x, Puntos.get(i+1).y);
            } catch (IndexOutOfBoundsException e) {
                g2d.drawLine(Puntos.get(i).x, Puntos.get(i).y, Puntos.get(0).x, Puntos.get(0).y);
            }
        }
    }
    
}
