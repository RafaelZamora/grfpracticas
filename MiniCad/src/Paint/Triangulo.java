/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Paint;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author reymi
 */
public class Triangulo extends Figura {

    Linea l1, l2, l3;
    Color color;

    public Triangulo(Linea l1, Linea l2, Linea l3, Color color) {
        this.l1 = l1;
        this.l2 = l2;
        this.l3 = l3;
        this.color = color;
    }

    @Override
    void Dibujar(Graphics2D g2d) {
        g2d.setColor(color);
        int[] xPoints = {l1.p1.x, l1.p2.x, l2.p1.x, l2.p2.x, l3.p1.x, l3.p2.x};
        int[] yPoints = {l1.p1.y, l1.p2.y, l2.p1.y, l2.p2.y, l3.p1.y, l3.p2.y};
        g2d.fillPolygon(xPoints, yPoints, 6);
    }
    
    @Override
    void Rotar(Graphics2D g2d){
        tetha += 15;
        g2d.rotate(Math.toRadians(tetha));
        Dibujar(g2d);
    }
    
    @Override
    void EscalarMas(Graphics2D g2d){
        zoom += 0.2d;
        g2d.scale(zoom, zoom);
        Dibujar(g2d);
    }
    
    @Override
    void EscalarMenos(Graphics2D g2d){
        zoom -= 0.2d;
        g2d.scale(zoom, zoom);
        Dibujar(g2d);
    }
}
