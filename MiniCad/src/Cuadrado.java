
import java.awt.Graphics2D;
import java.awt.Point;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author reymi
 */
public class Cuadrado extends Figura {
    Point Point1, Point2;
    
    public Cuadrado(Point p1, Point p2){
        this.Point1 = new Point(p1.x, p1.y);
        this.Point2 = new Point(p2.x, p2.y);
    }
    
    void dibujar(Graphics2D g2d){
        g2d.drawLine(Point1.x, Point1.y, Point2.x, Point2.y);
        g2d.drawLine(Point2.x, Point1.y, Point2.x, Point2.y);
        g2d.drawLine(Point1.x, Point2.y, Point1.x, Point2.y);
        g2d.drawLine(Point1.x, Point1.y, Point1.x, Point2.y);
    }
    
}
